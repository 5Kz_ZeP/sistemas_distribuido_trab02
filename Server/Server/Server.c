
#include<stdio.h>
#include<winsock2.h>
#include<pthread.h>

#pragma comment(lib,"ws2_32.lib") 
#pragma warning(disable : 4996) 

void Jogo(SOCKET *socket);

int main()
{
	WSADATA wsa; 
	SOCKET socketCommunication; 
	SOCKET ClientSocket;
	struct sockaddr_in server;
	struct sockaddr_in client;
	char* clientMessage, serverReply[2000];
	int ClientSize;
	int i = 0;
	pthread_t thread_id[5];

	//************************************************
	//******    INICIALIZAR SOCKET    ****************
	//************************************************
	printf("\nInicializar Socket...");
	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
	{
		printf("Erro ao Inicializar Socket - Error #%d \n", WSAGetLastError());
		return -1;
	}
	printf("\tSocket Inicializado.\n");


	//**********************************************
	//**********    CRIAR SOCKET    ****************
	//**********************************************
	//� Criado um socket e � verificado se este foi criado corretamente
	if ((socketCommunication = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET)
	{
		printf("Erro ao criar socket - Error #%d \n", WSAGetLastError());
		return -1;
	}
	printf("Socket Criado com sucesso.\n");

	server.sin_addr.s_addr = inet_addr("127.0.0.1");	//Indica o endere�o a que se ira ligar
	server.sin_family = AF_INET;						//Indica que � usado um o formato do IPV4
	server.sin_port = htons(8888);						//Indica a porta a que se ira ligar


	//**********************************************
	//**********         BIND       ****************
	//**********************************************
	//� feito o BIND do socket com a rede e verificado se o BIND foi realizado corretamente
	if (bind(socketCommunication, (struct sockaddr*) &server, sizeof(server)) == SOCKET_ERROR)
	{
		printf("Falha ao realizar Bind - Error #%d", WSAGetLastError());
	}
	puts("Bind Realizado com Sucesso");


	//**********************************************
	//**********       LISTEN       ****************
	//**********************************************
	//Indica que o socket recebe e escuta mensagens e que aceita ate 5 liga��es
	listen(socketCommunication, 5);


	puts("A Espera de conecoes...");




	//*****************************************
	//*******        THREADS        ***********
	//*****************************************
	/*pthread_t thread_id;
	pthread_create(&thread_id, NULL, Jogo, &ClientSocket);
	pthread_join(thread_id, NULL);*/


	//UMA VEZ QUE O SOCKET SO ACEITA APENAS 5 LIGA��ES RESTRINGIMOS O NUMERO DE ACEITA�OES MAXIMAS PARA 5
	//PODERIA SER USADO UM CONTADOR PARA DETERMINAR QUANTOS USER SE ENCONTRAM A COMUNICAR COM O SOCKET E ASSIM RESTRINGIR QUANDO SE DEVE PERMITIR A ENTRADA
	for (i = 0; i < 5; i++)
	{
		//**********************************************
		//**********         ACEPT       ***************
		//**********************************************
		//Aceita a liga�ao de um Client
		ClientSize = sizeof(client);
		ClientSocket = accept(socketCommunication, (struct sockaddr*) & client, &ClientSize);
		//Verifica se o Client foi aceite corretamente
		if (ClientSocket == INVALID_SOCKET)
		{
			printf("Falha ao Aceitar Cliente - Error #%d", WSAGetLastError());
			return -1;
		}
		else
		{
			//THREAD QUE SE ATIVARA PARA O JOGO DE CADA CLIENT
			if (pthread_create(&thread_id[i], NULL, Jogo, &ClientSocket) < 0)
			{
				printf("Erro ao criar THREAD");
				return -1;
			}

		}
	}
	pthread_join(thread_id[i], NULL);
	//***************************************************
	//**********       TERMINA SOCKET     ***************
	//***************************************************
	//Fecha o Socket
	closesocket(socketCommunication);
	//Termina Winsock
	WSACleanup();

	return 0;
}


void Jogo(SOCKET *socket) {

	//PEDRA - 0
	//PAPEL - 1
	//TESOURA - 2
	SOCKET ClientSocket = *socket;
	int BytesReceived;
	char message[2000];
	int jogar = 1;
	int Jogada_Server = 0;

	srand(time(NULL));
	printf("Um novo Client conectou-se \n");
	//REPLY TO CLIENTE
	//MENSAGEM DE ENVIADA QUANDO O CLIENTE SE CONECTA AO SERVIDOR
	strcpy(message, "100 OK\n");
	send(ClientSocket, message, strlen(message) + 1, 0);
	while (jogar == 1)
	{
		
		//RECIBE MESSAGE
		//Verifica a mensagem recebida e verifica se esta foi recebida corretamente
		if ((BytesReceived = recv(ClientSocket, message, strlen(message), 0)) != SOCKET_ERROR)
		{
			//JOGADA RANDOM DO SERVIDOR
			Jogada_Server = rand() % 3;

			//VERIFICA A DERROTA DO CLIENTE
			if ( (strcmp(message, "PLAY ROCK") == 0 && Jogada_Server == 1) ||
				 (strcmp(message, "PLAY PAPER") == 0 && Jogada_Server == 2) ||
				 (strcmp(message, "PLAY SCISSORS") == 0 && Jogada_Server == 0))
			{
				//REPLY TO CLIENT
				strcpy(message, "300 RESULT SERVER WINS");
				send(ClientSocket, message, strlen(message) + 1, 0);
			}


			//VERIFICA A VITORIA DO CLIENTE
			if ((strcmp(message, "PLAY ROCK") == 0 && Jogada_Server == 2) ||
				(strcmp(message, "PLAY PAPER") == 0 && Jogada_Server == 0) ||
				(strcmp(message, "PLAY SCISSORS") == 0 && Jogada_Server == 1))
			{
				//REPLY TO CLIENT
				strcpy(message, "300 RESULT CLIENT WINS");
				send(ClientSocket, message, strlen(message) + 1, 0);
			}


			//VERIFICA EMPATE
			if ((strcmp(message, "PLAY ROCK") == 0 && Jogada_Server == 0) ||
				(strcmp(message, "PLAY PAPER") == 0 && Jogada_Server == 1) ||
				(strcmp(message, "PLAY SCISSORS") == 0 && Jogada_Server == 2))
			{
				//REPLY TO CLIENT
				strcpy(message, "300 DRAW");
				send(ClientSocket, message, strlen(message) + 1, 0);
			}
			
			//SERA VERIFICADO SE O CLIENTE QUER JOGAR NOVAMENTE
			//REPLY TO CLIENTE
			strcpy(message, "100 OK\n");
			send(ClientSocket, message, strlen(message) + 1, 0);
			//RECIBE MESSAGE
			//Verifica a mensagem recebida e verifica se esta foi recebida corretamente
			if ((BytesReceived = recv(ClientSocket, message, strlen(message), 0)) != SOCKET_ERROR)
			{
				if (strcmp(message, "400 BYE") == 0)
				{
					//REPLY TO CLIENT
					strcpy(message, "400 BYE");
					send(ClientSocket, message, strlen(message) + 1, 0);
					jogar = 0;
				}
			}
			else 
			{ 
				printf("A mensagem do cliente nao foi recebida corretamente \n"); 
				return -1;
			}
		}
		else 
		{
			printf("A mensagem do cliente nao foi recebida corretamente \n");
			return -1;
		}

	}
	printf("O Client desconectou-se \n");
	return 0;


}